import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { CurrencyRatesComponent } from './currency-rates/currency-rates.component';


const routes: Routes = [
  { path: '', redirectTo: 'convert-rates', pathMatch: 'full' },
  { path: 'convert-rates', component: WelcomeComponent },
  { path: 'currency-rates', component: CurrencyRatesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
