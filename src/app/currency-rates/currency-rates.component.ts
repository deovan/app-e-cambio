import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectorCurrencys, selectorAmounts } from '../store/selectors/currency.seltectors';
import { LoadCurrenciesAction } from '../store/actions/currency.actions';
import { SetAmountsAction } from '../store/actions/amount.actions';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-currency-rates',
  templateUrl: './currency-rates.component.html',
  styleUrls: ['./currency-rates.component.scss']
})
export class CurrencyRatesComponent implements OnInit {
  public codeRate$: Observable<any>;
  public amount$: Observable<any>;
  selected = new FormControl('');

  constructor(private store: Store<any>) {
    this.codeRate$ = store.pipe(select(selectorCurrencys));
    this.amount$ = store.pipe(select(selectorAmounts));
    this.setInitialValue();
  }

  ngOnInit() {
    this.store.dispatch(new LoadCurrenciesAction());
  }

  setBase(base) {
    this.store.dispatch(new SetAmountsAction(base));
  }

  private setInitialValue() {
    this.codeRate$.subscribe(s => {
      if (s && s.rates) {
        this.selected.setValue(s.rates.find(c => c.code === s.base));
      }
    });
  }
}
