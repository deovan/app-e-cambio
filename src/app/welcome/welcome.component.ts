import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectorCurrencys } from '../store/selectors/currency.seltectors';
import { LoadCurrenciesAction } from '../store/actions/currency.actions';
import { MatSelect, MatOption } from '@angular/material';
import { SetAmountsAction } from '../store/actions/amount.actions';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  @ViewChild('select1') selectOne: MatSelect;
  @ViewChild('input1') optionOne: MatOption;
  public amount$: Observable<any>;
  selected = new FormControl('');

  constructor(private store: Store<any>) {
    this.amount$ = this.store.select(selectorCurrencys); // ok
    this.setInitialValue();
  }

  ngOnInit() {
    this.store.dispatch(new LoadCurrenciesAction);
  }

  private setInitialValue() {
    this.amount$.subscribe(s => {
      if (s && s.rates) {
        this.selected.setValue(s.rates.find(c => c.code === s.base));
      }
    });
  }

  setBase(base) {
    console.log(base);
    this.store.dispatch(new SetAmountsAction(base));
  }
}

