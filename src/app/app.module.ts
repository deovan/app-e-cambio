import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WelcomeComponent } from './welcome/welcome.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { CurrencyEffects } from './store/effects/currency.effects';
import { CurrencyRatesComponent } from './currency-rates/currency-rates.component';
import {
  MatToolbarModule,
  MatCardModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatSelectModule,
  MatInputModule,
  MatGridListModule,
  MatListModule,
  MatFormFieldModule
} from '@angular/material';
import { CurrencyStateModule } from './store/selectors/currency.seltectors';
import {  StoreModule } from '@ngrx/store';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    MainNavComponent,
    CurrencyRatesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatSelectModule,
    MatInputModule,
    MatGridListModule,
    MatListModule,
    MatFormFieldModule,
    CurrencyStateModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([CurrencyEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
