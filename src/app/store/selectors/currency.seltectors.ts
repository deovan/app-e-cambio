import {
  createSelector,
  StoreModule
} from '@ngrx/store';
import { currencyReducer } from '../reducers/currency.reducer';
import { amountReducer } from '../reducers/amount.reducer';


export const CurrencyStateModule = StoreModule.forFeature('currency', {
  currencys: currencyReducer,
  amounts: amountReducer
});

export interface CurrencyState {
  currencys: any;
  amounts: any;
}

export const selectCurrency = state => state.currency;

export const selectorCurrencys =
  createSelector(selectCurrency, (state: CurrencyState) => {
    console.log('selector', state.currencys);
    return state.currencys;
  });

export const selectorAmounts =
  createSelector(selectCurrency, (state: CurrencyState) => state.amounts);

