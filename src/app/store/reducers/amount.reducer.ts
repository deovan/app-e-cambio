import { SetAmountsAction, SET_AMOUNTS } from '../actions/amount.actions';

export function amountReducer(
  state = 'USD',
  action: SetAmountsAction) {
  switch (action.type) {
    case SET_AMOUNTS:
      return action.payload;
    default:
      return state;
  }
}
