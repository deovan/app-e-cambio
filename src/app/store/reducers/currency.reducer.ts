import { SetCurrenciesAction, SET_CURRENCIES } from '../actions/currency.actions';

export function currencyReducer(
  state = {},
  action: SetCurrenciesAction) {
  switch (action.type) {
    case SET_CURRENCIES:
      console.log('action', action);
      return action.payload;
    default:
      return state;
  }
}
