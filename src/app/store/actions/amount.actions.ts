import { Action } from '@ngrx/store';

export const SET_AMOUNTS = '[Amount] Set Amounts';

export class SetAmountsAction implements Action {
  readonly type = SET_AMOUNTS;
  constructor(public payload: string) { }
}
