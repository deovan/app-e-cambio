import { Action } from '@ngrx/store';

export const LOAD_CURRENCIES = '[Currency] Load Currencies';
export const SET_CURRENCIES = '[Currency] Set Cunrrencies';

export class LoadCurrenciesAction implements Action {
  readonly type = LOAD_CURRENCIES;
}

export class SetCurrenciesAction implements Action {
  readonly type = SET_CURRENCIES;
  constructor(public payload: any) { }
}
