import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { combineLatest } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SetCurrenciesAction, LoadCurrenciesAction, LOAD_CURRENCIES } from '../actions/currency.actions';
import { Store } from '@ngrx/store';
import { switchMap, map, combineAll } from 'rxjs/operators';
import { selectorAmounts } from '../selectors/currency.seltectors';

@Injectable()
export class CurrencyEffects {
  keyAlpha = 'SIQK6CUXOJ2FG5UX';
  public base;
  public flag;

  constructor(
    private store: Store<any>,
    private actions$: Actions,
    private http: HttpClient) { }

  @Effect()
  currencyAPI$: Observable<any> =
    combineLatest(
      this.actions$.ofType(LOAD_CURRENCIES),
      this.store.select(selectorAmounts)
    ).pipe(
      switchMap(([_, base]) => {
        console.log('base', base);
        this.base = base ? base : 'USD';
        return this.http.get<any>(`https://api.exchangeratesapi.io/latest?base=${this.base}`);
      }),
      map((resp) => {
        console.log('res', resp);

        const codeCountry = CODE_COUNTRY;
        for (let index = 0; index < codeCountry.rates.length; index++) {
          const ratesCode = (codeCountry.rates[index].code);
          codeCountry.rates[index].rate = resp.rates[ratesCode].toFixed(3);
        }
        codeCountry.date = resp.date;
        codeCountry.base = resp.base;

        codeCountry.rates.sort(function (a, b) {
          if (a.code < b.code) { return -1; }
          if (a.code > b.code) { return 1; }
          return 0;
        });
        return new SetCurrenciesAction(codeCountry);
      })
    );
}

const CODE_COUNTRY = {
  rates: [{
    code: 'BGN',
    country: 'Bulgária',
    currency: 'Lev búlgaro',
    simbolo: 'Лв', rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Bulgaria.svg/120px-Flag_of_Bulgaria.svg.png'
  },
  {
    code: 'CAD',
    country: 'Canadá',
    currency: 'Dólar canadense',
    simbolo: 'C$',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Flag_of_Canada.svg/120px-Flag_of_Canada.svg.png'
  },
  {
    code: 'BRL',
    country: 'Brasil',
    currency: 'Real brasileiro',
    simbolo: 'R$',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Flag_of_Brazil.svg/120px-Flag_of_Brazil.svg.png'
  },
  {
    code: 'HUF',
    country: 'Hungria',
    currency: 'Forint',
    simbolo: 'Ft', rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Flag_of_Hungary.svg/120px-Flag_of_Hungary.svg.png'
  },
  {
    code: 'DKK',
    country: 'Dinamarca',
    currency: 'Coroa dinamarquesa',
    simbolo: 'kr.',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Flag_of_Denmark.svg/120px-Flag_of_Denmark.svg.png'
  },
  {
    code: 'JPY',
    country: 'Japão',
    currency: 'Iene',
    simbolo: '¥',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Flag_of_Japan.svg/120px-Flag_of_Japan.svg.png'
  },
  {
    code: 'ILS',
    country: 'Israel',
    currency: 'Novo Sheqel israelense',
    simbolo: '₪',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Israel.svg/120px-Flag_of_Israel.svg.png'
  },
  {
    code: 'TRY',
    country: 'Turquia',
    currency: 'Lira turca',
    simbolo: '₺',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Flag_of_Turkey.svg/120px-Flag_of_Turkey.svg.png'
  },
  {
    code: 'RON',
    country: 'Romênia',
    currency: 'Leu romeno',
    simbolo: 'L',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Flag_of_Romania.svg/120px-Flag_of_Romania.svg.png'
  },
  {
    code: 'GBP',
    country: 'Grã-Bretanha',
    currency: 'Libra esterlina',
    simbolo: '£',
    rate: '',
    // tslint:disable-next-line:max-line-length
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/120px-Flag_of_the_United_Kingdom.svg.png'
  },
  {
    code: 'PHP',
    country: 'Filipinas',
    currency: 'Peso filipino',
    simbolo: '₱',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Flag_of_the_Philippines.svg/120px-Flag_of_the_Philippines.svg.png'
  },
  {
    code: 'HRK',
    country: 'Croácia',
    currency: 'Kuna',
    simbolo: 'Kn',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Flag_of_Croatia.svg/120px-Flag_of_Croatia.svg.png'
  },
  {
    code: 'NOK',
    country: 'Noruega',
    currency: 'Coroa norueguesa',
    simbolo: 'Kn',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Norway.svg/120px-Flag_of_Norway.svg.png'
  },
  {
    code: 'ZAR',
    country: 'África do Sul',
    currency: 'Rands sul-africano',
    simbolo: 'R',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Flag_of_South_Africa.svg/120px-Flag_of_South_Africa.svg.png'
  },
  {
    code: 'MXN',
    country: 'México',
    currency: 'Peso mexicano',
    simbolo: '$',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Mexico.svg/120px-Flag_of_Mexico.svg.png'
  },
  {
    code: 'AUD',
    country: 'Austrália',
    currency: 'Dólar australiano',
    simbolo: '$',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Flag_of_Australia.svg/120px-Flag_of_Australia.svg.png'
  },
  {
    code: 'USD',
    country: 'Estados Unidos',
    currency: 'Dólar americano',
    simbolo: '$',
    rate: '',
    flag: 'https://www.sogeografia.com.br/figuras/bandeiras/america/mini/estadosunidos.jpg'
  },
  {
    code: 'KRW',
    country: 'Coréia do Sul',
    currency: 'Won sul-coreano',
    simbolo: '₩',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Flag_of_South_Korea.svg/120px-Flag_of_South_Korea.svg.png'
  },
  {
    code: 'HKD',
    country: 'Hong Kong',
    currency: 'Dólar de Hong Kong',
    simbolo: 'HK$',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/5/5b/Flag_of_Hong_Kong.svg'
  },
  {
    code: 'EUR',
    country: 'União Européia',
    currency: 'Euro',
    simbolo: '€',
    rate: '',
    flag: 'http://geo5.net/imagens/bandeira-da-europa-350x233.png'
  },
  {
    code: 'ISK',
    country: 'Islândia',
    currency: 'Coroa da Islândia',
    simbolo: 'Íkr',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Flag_of_Iceland.svg/120px-Flag_of_Iceland.svg.png'
  },
  {
    code: 'CZK',
    country: 'República Checa',
    currency: 'Coroa Tcheca',
    simbolo: 'Kč',
    rate: '',
    // tslint:disable-next-line:max-line-length
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Flag_of_the_Czech_Republic.svg/120px-Flag_of_the_Czech_Republic.svg.png'
  },
  {
    code: 'THB',
    country: 'Tailândia',
    currency: 'Baht',
    simbolo: '฿',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Flag_of_Thailand.svg/120px-Flag_of_Thailand.svg.png'
  },
  {
    code: 'MYR',
    country: 'Malásia',
    currency: 'Ringgit da Malásia',
    simbolo: 'RM',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Flag_of_Malaysia.svg/120px-Flag_of_Malaysia.svg.png'
  },
  {
    code: 'NZD',
    country: 'Nova Zelândia',
    currency: 'Dólar da Nova Zelândia',
    simbolo: '$',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Flag_of_New_Zealand.svg/120px-Flag_of_New_Zealand.svg.png'
  },
  {
    code: 'PLN',
    country: 'Polônia',
    currency: 'Zloty',
    simbolo: 'zł',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Flag_of_Poland.svg/120px-Flag_of_Poland.svg.png'
  },
  {
    code: 'CHF',
    country: 'Suiça',
    currency: 'Franco suíço',
    simbolo: 'SFr',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Switzerland.svg/120px-Flag_of_Switzerland.svg.png'
  },
  {
    code: 'SEK',
    country: 'Suécia',
    currency: 'Swedish Krona',
    simbolo: 'kr',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Flag_of_Sweden.svg/120px-Flag_of_Sweden.svg.png'
  },
  {
    code: 'CNY',
    country: 'China',
    currency: 'Yuan Renminbi',
    simbolo: '¥',
    rate: '',
    // tslint:disable-next-line:max-line-length
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_the_People%27s_Republic_of_China.svg/120px-Flag_of_the_People%27s_Republic_of_China.svg.png'
  },
  {
    code: 'SGD',
    country: 'Singapura',
    currency: 'Dólar de Singapura',
    simbolo: 'S$',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Flag_of_Singapore.svg/120px-Flag_of_Singapore.svg.png'
  },
  {
    code: 'INR',
    country: 'Índia',
    currency: 'Rupia indiana',
    simbolo: 'Rs.',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_India.svg/120px-Flag_of_India.svg.png'
  },
  {
    code: 'IDR',
    country: 'Indonésia',
    currency: 'Rupia',
    simbolo: 'Rp',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/120px-Flag_of_Indonesia.svg.png'
  },
  {
    code: 'RUB',
    country: 'Rússia',
    currency: 'rublo russo',
    simbolo: '₽',
    rate: '',
    flag: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Russia.svg/120px-Flag_of_Russia.svg.png'
  }],
  date: '',
  base: ''
};
